const express = require('express');
const routerEatingValidator = require('express').Router();
var moment = require('moment');
var eatingErrorCodes = require('../model/eatingerrorcodes.json');

routerEatingValidator.post('/eatingvalidator', function(req, res) {
  console.log(req.body);
  var messageType = parseInt(String(req.body.messageType));
  var lang = req.body.lang.substring(0,3);
  var userLang = lang=="eng" ? "" : "." + lang;
  var userHeight = req.body["user.height"];
  var userWeight = req.body["user.weight"]
  var block = req.body.block;



//for height
if(messageType == 1) {
    try {
      if (!isNaN(userHeight)) {
        console.log("userHeight if")
        var height = parseFloat(userHeight).toFixed(2);
        if (height > 230.0 ) {
          jsonResponse = setErrorResponse("height", eatingErrorCodes[lang]["err5"]);
        }
        else if (height < 50.0) {
          jsonResponse = setErrorResponse("height", eatingErrorCodes[lang]["err6"]);
        }
        else {
          jsonResponse = setHeightResponse(block,height);
        }
      }
      else {
        jsonResponse = setErrorResponse("height", eatingErrorCodes[lang]["err2"]);
      }
    }
    catch(e) {
      console.log(e);
      jsonResponse = setErrorResponse("height", eatingErrorCodes[lang]["err2"]);
    }

  }

//for weight
  else if(messageType == 2) {
    try {
      if (!isNaN(userWeight)) {
        var weight = parseFloat(userWeight).toFixed(2);
        if (weight > 300 ) {
          jsonResponse = setErrorResponse("weight", eatingErrorCodes[lang]["err7"])
        }
        else if (weight < 2) {
          jsonResponse = setErrorResponse("weight", eatingErrorCodes[lang]["err8"])
        }
        else {
          jsonResponse = setWeightResponse(block,weight);
        }
      }
      else {
        jsonResponse = setErrorResponse("weight", eatingErrorCodes[lang]["err2"]);
      }
    }
    catch(e) {
      console.log(e);
      jsonResponse = setErrorResponse("weight", eatingErrorCodes[lang]["err2"]);
    }
  }

  function setHeightResponse(block,height){
    return {
      "redirect_to_blocks" : [block+ userLang],
      "set_attributes" : {
        "user.height" : height
      }
    }
  }

  function setWeightResponse(block,weight){
    return {
      "redirect_to_blocks" : [block + userLang],
      "set_attributes" : {
        "user.weight" : weight
      }
    }
  }

  function setErrorResponse(block, message, ref) {

    return !ref ? 
    {
      "redirect_to_blocks": [block + ".eating.fallback.error" + userLang],
      "messages": [{
        "text":message
      }]
    }:
    {
      "redirect_to_blocks": [block + ".eating.fallback.error" + userLang],
      "messages": [{
        "text":message
      }]
    }
  }
  res.send(jsonResponse); 
});

module.exports = routerEatingValidator ;