const express = require("express");
const router = express.Router();
const { checkDiabetes } = require("../controller/diabetes.js")

router.route("/").post(checkDiabetes)

module.exports = router