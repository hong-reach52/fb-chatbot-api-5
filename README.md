## About The Project

Reach52 backend API to interact any chatbot webhooks like chatfuel.com and turn.io

### Built With

-   [Express.js](https://expressjs.com/)
-   [ngrok](https://ngrok.com/)
-   [axios](https://www.npmjs.com/package/axios)
-   [node + npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

Before anything else lets first install npm

-   npm
    ```sh
    download and install latest node and npm https://docs.npmjs.com/downloading-and-installing-node-js-and-npm
    ```
-   ngrok local machine
    ```sh
    download and install latest https://ngrok.com/download
    ```
-   or ngrok VS code extension
    ```sh
    search "ngrok Fro VSCode" author "philnash" VSCode id "philnash.ngrok-for-vscode"
    ```

### Installation

1. Clone the repo
    ```sh
    git clone https://bitbucket.org/reach52/fb-chatbot-api/src/master/
    ```
2. Install npm packages
    ```sh
    npm install
    ```

## Usage

Run the application

1. On the root folder 2 option to run use nodemon if you have installed locally if not see this link https://www.npmjs.com/package/nodemon or run npm script
   **nodemon**
    ```sh
    nodemon index.js
    ```
    **npm script**
    ```sh
    npm run start
    ```

## Deployment

Please contact the current server administrator for deployment

Following are the deployment steps:

1. Clone repo
    ```sh
    git clone https://r52-bart@bitbucket.org/reach52/fb-chatbot-api.git
    ```
2. Install NPM and dependencies
    ```sh
    sudo npm install -g
    ```
    ```sh
    sudo npm install cross-env
    ```
    ```sh
    sudo npm install nodemon
    ```
3. install PM2
    ```sh
    sudo npm install pm2 -g
    ```
4. start the API

    ```sh
    pm2 start index.js --name "chatbot-api"
    ```

5. additional infra setup

    - register sub-domain with DNS service provider
    - publish reverse proxy with nginx
    - register SSL certificates with certbot

## Roadmap

See the [open issues](https://bitbucket.org/reach52/fb-chatbot-api/jira?statuses=new&statuses=indeterminate&sort=-updated&page=1) for a list of proposed features (and known issues).

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

## Contact

-   Robert Janagap - [@Linkedin](https://www.linkedin.com/in/robert-janagap/)
-   Bartson Batobato - [@Email](bart@reach52.com)

Project Link: [https://bitbucket.org/reach52/fb-chatbot-api/src/master/](https://bitbucket.org/reach52/fb-chatbot-api/src/master/)
